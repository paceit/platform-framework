<?php namespace Platform\Framework\Http\Middleware;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Closure;

class AddRequestUrlHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure                  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('X-Request-URL', trim($_SERVER['REQUEST_URI'], '/'));
        return $response;
    }
}
