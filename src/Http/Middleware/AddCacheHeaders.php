<?php namespace Platform\Framework\Http\Middleware;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Closure;

class AddCacheHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure                  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // If cache is enabled, add header
        if (app()->cacheActive() === true) {
            $expires = gmdate('D, d M Y H:i:s \G\M\T', time() + 300);
            $response->header('cache-control', 'public');
            $response->header('expires', $expires);

            // Remove cookies
            $headers = $response->headers;
            $cookies = $headers->getCookies();

            foreach ($cookies as $cookie) {
                $headers->removeCookie(
                    $cookie->getName(),
                    $cookie->getPath(),
                    $cookie->getDomain()
                );
            }
        }

        return $response;
    }
}
