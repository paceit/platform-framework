<?php namespace Platform\Framework\Http;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Allsop\App\Models\GroupRouteRoutePermission;
use Allsop\App\Models\Route;
use Allsop\App\Models\RoutePermission;
use Laravel\Lumen\Routing\Controller as RootController;

class Controller extends RootController
{
    /**
     * Call API version of route and return response.
     *
     * @param  string $name
     * @param  array  $params
     * @param  array  $request
     * @param  bool   $transform
     * @return mixed
     */
    protected function data($name = null, $params = null, $request = null, $transform = true)
    {
        return $this->api()->getRouteData($name, $params, $request, $transform);
    }

    /**
     * Check if user has requested permission.
     * 
     * @param  string $permission
     * @param  string $routeName
     * @return boolean
     */
    protected function hasPermission($permission, $routeName = null)
    {
        // Retrieve permissions
        $permissions = app()->getPermissions($routeName);
        if ($permissions === false) {
            return false;
        }

        return isset($permissions[$permission]) && $permissions[$permission] === true;
    }

    /**
     * Check if user has requested permissions.
     * 
     * @return boolean
     */
    protected function hasPermissions()
    {
        $permissions = func_get_args();

        // Loop through requested permissions and return
        // false if any are not enabled
        foreach ($permissions as $permission) {
            if (! $this->hasPermission($permission)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set resource handler for current request.
     *
     * @param  string $resoruce
     * @return void
     */
    protected function setResource($resource)
    {
        $this->api()->setResource($resource);
    }

    /**
     * Set transformer to use for current request.
     * 
     * @param  \League\Fractal\TransformerAbstract $transformer
     * @return void
     */
    protected function setTransformer(TransformerAbstract $transformer)
    {
        $this->api()->setTransformer($transformer);
    }

    /**
     * Return API handler.
     * 
     * @return Platform\Framework\Contracts\Api
     */
    protected function api()
    {
        return app('api');
    }
}
