<?php namespace Platform\Framework\Http;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Throw an error exception.
     *
     * @param string $error
     * @param int    $code
     */
    protected function error($error, $code = 500)
    {
        // If error is an array, we should be using errors method
        if (is_array($error)) {
            $this->errors($error, $code);
        }

        throw new HttpResponseException(
            $this->buildJsonResponseError($error, $code, 'error')
        );
    }

    /**
     * Throw an errors exception.
     *
     * @param array $errors
     * @param int   $code
     */
    protected function errors($errors, $code = 500)
    {
        // If error is not an array, we should be using error method
        if (! is_array($errors)) {
            $this->error($errors, $code);
        }

        throw new HttpResponseException(
            $this->buildJsonResponseError($errors, $code)
        );
    }

    /**
     * Create the response for when a request fails validation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array                     $errors
     * @return \Illuminate\Http\Response
     */
    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        return $this->buildJsonResponseError($errors, 422);
    }

    /**
     * Buid JSON error response.
     * 
     * @param  string|array $errors
     * @param  int          $code
     * @param  string       $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function buildJsonResponseError($errors, $code, $key = 'errors')
    {
        return new JsonResponse([$key => $errors], $code);
    }
}
