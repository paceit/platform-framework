<?php namespace Platform\Framework;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Dotenv\Loader as Loader;

class Dotenv
{
    /**
     * Load envrionment variables by parsing .env files if a given
     * path (root .env and platform environment specific, e.g.
     * staging.env).
     *
     * @return void
     */
    public static function load($path, $file = null)
    {
        $path = rtrim($path, '/').'/';

        // If no file is specified, check for environment
        // specific file and root file
        if (is_null($file)) {
            $env = getenv('PLATFORM_ENV');

            // Do we have a valid environment config?
            if ($env !== null) {
                $filePath = $path.$env.'.env';

                // If a valid file exists, load variables
                if (is_file($filePath)) {
                    (new Loader($filePath, true))->load();
                }
            }

            // Load base config
            (new Loader($path.'.env', true))->load();
            return;
        }

        // Load specific file
        (new Loader($path.$file, true))->load();
    }
}
