<?php namespace Platform\Framework\Providers;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Illuminate\View\ViewServiceProvider as RootServiceProvider;
use Illuminate\View\FileViewFinder;
use Platform\Framework\View\Engines\ReactEngine;
use Platform\Framework\View\Engines\ReactEngineException;

class ViewServiceProvider extends RootServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        $this->registerReactEngine();

        // Register react extension
        $app = $this->app;
        $app['view']->addExtension('jsx', 'jsx', function() use ($app)
        {
            return $app['react.engine'];
        });
    }

    /**
     * Register react engine to container.
     *
     * @return void
     */
    public function registerReactEngine()
    {
        $this->app->singleton('react.engine', function($app)
        {
            $request = $app['request'];
            $route   = $request->getRouteResolver();
            $uri     = $request->path();

            // Add query string to URI
            $query = $request->getQueryString();
            if ($query) {
                $uri .= '?'.$query;
            }

            // Parse no scripts flag
            $noScripts = $request->get('noscripts') === 'true';

            // Instantiate engine and set renderer socket
            $engine = new ReactEngine($app, $uri, $route()[1], true, $noScripts);
            $engine->setSocket(env('REACT_SOCKET', 'unix:///tmp/node/node.sock'));

            return $engine;
        });
    }

    /**
     * Register the view finder implementation. Overriding this so it becomes
     * a singleton (so we can use it within the react engine).
     *
     * @return void
     */
    public function registerViewFinder()
    {
        $this->app->singleton('view.finder', function($app)
        {
            $paths = $app['config']['view.paths'];

            return new FileViewFinder($app['files'], $paths);
        });
    }
}
