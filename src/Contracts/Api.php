<?php namespace Platform\Framework\Contracts;

/**
 * Platform: Franework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use League\Fractal\TransformerAbstract;

interface Api
{
    /**
     * Return API data for current route.
     *
     * @param  string $name
     * @param  array  $params
     * @param  array  $request
     * @param  bool   $transform
     * @return mixed
     */
    public function getRouteData($name = null, $params = null, $request = null, $transform = true);

    /**
     * Run the passed response through a matching transformer.
     * 
     * @param  mixed $response
     * @return mixed
     */
    public function transform($response);

    /**
     * Register transformer.
     *
     * @param  string $transform
     * @param  string $transform
     * @return void
     */
    public function registerTransformer($transform, $transformer);

    /**
     * Set default collection resource handler.
     * 
     * @param  string $resource
     * @return void
     */
    public function setDefaultCollectionResource($resource);

    /**
     * Set default item resource handler.
     * 
     * @param  string $resource
     * @return void
     */
    public function setDefaultItemResource($resource);

    /**
     * Set transformer to use when no specific transformer is available.
     * 
     * @param  string $transformer
     * @return void
     */
    public function setDefaultTransformer($transformer);

    /**
     * Set resource handler for current request.
     *
     * @param  string $resoruce
     * @return void
     */
    public function setResource($resource);

    /**
     * Set transformer to use for current request.
     * 
     * @param  \League\Fractal\TransformerAbstract $transformer
     * @return void
     */
    public function setTransformer(TransformerAbstract $transformer);
}
