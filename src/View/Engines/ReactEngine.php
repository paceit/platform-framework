<?php namespace Platform\Framework\View\Engines;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Exception;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\View\Engines\Engine;
use Illuminate\View\Engines\EngineInterface;
use Illuminate\View\Engines\PhpEngine;
use Platform\App\Models\ParameterValue;

class ReactEngine extends Engine implements EngineInterface
{
    /**
     * App instance
     *
     * @var \Illuminate\Container\Container
     */
    protected $app;
    
    /**
     * Currently open socket
     *
     * @var Resource
     */
    protected $openSocket = false;

    /**
     * Sources required for current request.
     *
     * @var array
     */
    protected $required = [];

    /**
     * Current route configuration
     *
     * @var array
     */
    protected $routeConfig;

    /**
     * Unix socket path
     *
     * @var string
     */
    protected $socket;

    /**
     * State cache
     *
     * @var array
     */
    protected $stateCache;

    /**
     * Strip scripts from response.
     *
     * @var bool
     */
    protected $stripScripts;

    /**
     * URI (with query string) of current request
     *
     * @var string
     */
    protected $uri;

    /**
     * Use cache when compiling
     *
     * @var bool
     */
    protected $useCache;

    /**
     * Construct class, receiving finder instance, current request
     * and current route.
     *
     * @param \Illuminate\Container\Container $app
     * @param string $uri
     * @param array  $routeConfig
     * @param boool  $useCache
     */
    public function __construct(Container $app, $uri, $routeConfig, $useCache = true, $stripScripts = false)
    {
        $this->app          = $app;
        $this->routeConfig  = $routeConfig;
        $this->useCache     = $useCache;
        $this->stripScripts = $stripScripts;

        // Parse URI into format required by react router
        $this->uri = trim($uri, '/');
    }

    /**
     * Get the rendered contents of a component.
     *
     * @param  string  $path
     * @param  array   $props
     * @
     * @return string
     */
    public function get($path, array $props = array())
    {
        return $this->compile($this->parseData($path, $props), $props);
    }

    /**
     * Get socket.
     *
     * @return string
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * Set socket.
     *
     * @param  string $socket
     * @return void
     */
    public function setSocket($socket)
    {
        $this->socket = $socket;
    }
    
    /**
     * Close open socket.
     *
     * @return void
     */
    protected function closeSocket()
    {
        // Skip if not open
        if ($this->openSocket === false) {
            return;
        }
        
        try {
            fclose($this->openSocket);
        } catch (Exception $e) {
            // We don't actually care it fails to close, it's probably
            // been closed by the other side and doesn't impact on the
            // data
        }
    }
    
    /**
     * Check for cached response.
     *
     * @param  string $sourcePath
     * @param  string $viewPath
     * @param  array  $data
     * @param  array  $props
     * @return string
     */
    protected function checkCache($sourcePath, $viewPath, &$data, $props)
    {
        try {

            // If we are not using cache, return false
            if ($this->useCache === false) {
                return false;
            }
            
            // If we are in local mode, return false
            $environment = env('PLATFORM_ENV', 'production');
            if ($environment === 'local') {
                return false;
            }
            
            // Attempt to load source cache
            $required = file_get_contents($sourcePath);
            if ($required === false) {
                return false;
            }
            
            $this->required = json_decode($required, true);
            
            // Loop through required data, run requests and add to state
            foreach ($this->required as $path) {
                $path = trim($path, '/');
                $data['state'][$path] = $this->executeRequest($path);
            }
            
            // Attempt to load view cache
            $cache = file_get_contents($viewPath);
            if ($cache === false) {
                return false;
            }
            
            $cache = json_decode($cache, true);
            
            // Parse data key
            $key = json_encode([
                'data'  => $data,
                'props' => $props,
            ]);
            
            // If we have a match, return cache
            if ($cache['key'] === $key) {
                return $cache;
            }
        }
        
        // Catch any exceptions through by file system requests,
        // such as when the file doesn't exist
        catch(Exception $e) {
            //
        }
        
        // No match, return false
        return false;
    }

    /**
     * Compile view from passed data.
     *
     * @param  string $data
     * @param  array  $props
     * @return string
     */
    protected function compile($data, $props = array())
    {
        try {

            // Load template
            $template = $this->app['view.finder']->find($this->routeConfig['template']);
            if (! is_file($template)) {
                throw new ReactEngineException("Template path invalid ({$config['template']})");
            }
            
            // Parse cache paths
            $cachePath    = base_path('storage/cache/');
            $sourcePath   = $cachePath.'views/sources/'.md5($data['pattern']).'.json';
            $viewPath     = $cachePath.'views/compiled/'.md5($data['uri']).'.json';

            // Store state in cache
            $this->stateCache = $data['state'];
            
            // Retrieve cache
            $cache = $this->checkCache($sourcePath, $viewPath, $data, $props);
            if ($cache !== false) {   

                // If response is cacheable, enable cache
                if (env('PLATFORM_ENV', 'production') !== 'local' && $cache['cache'] === true) {
                    $this->app->enableCache();
                }

                // Return response
                return $cache['response'];
            }
            
            // Send compile request
            $response = $this->send($data);
            
            // If data is required, retrieve and add to state. As child
            // components can depend on parents having data, this may
            // run multiple times.
            while (substr($response, 0, 3) === 'r||') {
                $this->required = json_decode(substr($response, 3, -2), true);
                $data['state']  = [];

                // Build state data
                foreach ($this->required as $path) {
                    $path = trim($path, '/');

                    // Missing state, execute internal request
                    if (! isset($this->stateCache[$path])) {
                        $this->stateCache[$path] = $this->executeRequest($path);
                    }

                    // Add to state response
                    $data['state'][$path] = $this->stateCache[$path];
                }

                // Send new compile request
                $response = $this->send([
                    'state' => $data['state']
                ]);
            }
            
            // If markup has not been returned, throw exception
            $type = substr($response, 0, 3);
            if ($type !== 'm||' && $type !== 'c||') {
                throw new ReactEngineException('Unexpected response from socket');
            }
            
            // Attempt to cache sources
            if ($this->useCache === true && @file_put_contents($sourcePath, json_encode($required)) === false) {
                throw new ReactEngineException('Unable to cache required sources');
            }
            
            // Close socket
            $this->closeSocket();
            
            // Find and replace build files
            $response = $this->replaceBuild($response);
            
            // Parse asset base
            $assetBase = env('ASSET_BASE');
            if (!is_null($assetBase)) {
                $assetBase = '\''.$assetBase.'\'';
            } else {
                $assetBase = 'null';
            }

            // Parse media base
            $mediaBase = env('MEDIA_BASE');
            if (!is_null($mediaBase)) {
                $mediaBase = '\''.$mediaBase.'\'';
            } else {
                $mediaBase = 'null';
            }

            // Parse template
            $response = (new PhpEngine)->get($template, [
                'component' => substr($response, 3, -2),
                'header'    => $props['_header'],
                'named'     => json_encode($data['named']),
                'state'     => json_encode($data['state']),
                
                'assetBase' => $assetBase,
                'mediaBase' => $mediaBase,
            ]);

            // Strip whitespace
            $response = preg_replace('~>\\s+<~m', '><', $response);

            // If we have been told to render no scripts, strip from response
            if ($this->stripScripts === true) {
                $response = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $response);
            }
            
            // Build cache
            $cache = [
                'cache'     => $type === 'c||',
                'response'  => $response,
                'key'       => json_encode([
                    'data'  => $data,
                    'props' => $props,
                ]),
            ];
            
            // Update cache
            if ($this->useCache === true && @file_put_contents($viewPath, json_encode($cache)) === false) {
                throw new ReactEngineException('Unable to cache compiled view');
            }

            // If response is cacheable, enable cache
            if ($cache['cache'] === true) {
                $this->app->enableCache();
            }
            
            // Return buffer
            return $response;
        
        // Rethrow exceptions as react engine exception
        } catch(Exception $e) {
            $this->closeSocket();
            throw new ReactEngineException($e);
        }
    }
    
    /**
     * Execute request.
     *
     * @param  string $path
     * @return array
     */
    protected function executeRequest($path)
    {
        // Set internal request
        $this->app->setInternalRequest();

        // Build request
        $originalRequest = $this->app['request'];
        $request = Request::createFromBase($originalRequest);
        $request->server->set('REQUEST_URI', '/'.$path);
        
        // Build query string array
        $parts = parse_url($path);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
        } else {
            $query = [];
        }
        
        $query['react'] = time() * 1000;
        
        // Reset so new URI is picked up
        $request->initialize(
            $query,
            $query,
            $request->attributes->all(),
            $request->cookies->all(),
            [],
            $request->server->all()
        );
        
        // Add react header
        $request->headers->set('X-Powered-By', 'React');
        
        // Run request
        $response = $this->app->dispatch($request);
        $status   = $response->getStatusCode();

        // Restore original request
        $this->app->instance('Illuminate\Http\Request', $originalRequest);
        
        // If response is not 200 (OK), throw exception
        if ($status !== 200) {
            throw new ReactEngineException('One or more sub-components returned status '.$status);
        }
        
        // Attempt to decode response
        $content = json_decode($response->getContent(), true);
        if ($content === false) {
            throw new ReactEngineException('Unable to parse response for one more sub-components');
        }

        $this->app->setInternalRequest(false);
        return $content;
    }

    /**
     * Parse data and paths into schema needed by socket.
     *
     * @param  string $path
     * @param  array  $state
     * @return string
     */
    protected function parseData($path, $state)
    {
        $config = $this->routeConfig;
        $named  = [];
        
        // Parse template
        if (! isset($config['template'])) {
            throw new ReactEngineException('No template specified');
        }
        
        // Parse routes location
        if (! isset($config['package'])) {
            throw new ReactEngineException('No package specified on route');
        }
        
        $routes = str_replace('\\', '/', strtolower($config['package']));
        $routes = base_path().'/packages/'.$routes.'/resources/react/Routes.jsx';

        // Remove unncessary data
        foreach (array('__env', '_header', 'app') as $key) {
            if (isset($state[$key])) {
                unset($state[$key]);
            }
        }

        // Convert format for use server server state store
        $state = [$this->uri => $state];

        // Retrieve named routes
        $values = ParameterValue::where('parameter_code', 'ROUTES');
        if ($values->count() > 0) {
            $values = $values->get();

            // Loop through routes
            foreach ($values as $value) {
                
                // Create named array
                if (! isset($named[$value['primary']])) {
                    $named[$value['primary']] = [];
                }

                $named[$value['primary']][$value['parameter_rule_key']] = $value['value'];
            }
        }

        // Return data
        return [
            'assetBase' => env('ASSET_BASE'),
            'mediaBase' => env('MEDIA_BASE'),
            'named'     => array_values($named),
            'pattern'   => $config['path'],
            'routes'    => $routes,
            'state'     => $state,
            'uri'       => $this->uri,
        ];
    }
    
    /**
     * Replace file paths with build paths.
     *
     * @param  string $reponse
     * @return string
     */
    protected function replaceBuild($response)
    {
        $path = base_path('public').'/build/rev-manifest.json';

        // If we have manifest, return
        if (! is_file($path)) {
            return $response;
        }
        
        $contents = file_get_contents($path);
        $manifest = json_decode($contents, true);
        
        // Loop through values and prefix with build
        foreach ($manifest as &$value) {
            $value = 'build/'.$value;
        }
        
        // Mass find and replace
        return str_replace(array_keys($manifest), $manifest, $response);
    }
    
    /**
     * Send data through socket and return response.
     *
     * @param  string   $data
     * @return string
     */
    protected function send($data)
    {
        try {

            // Attempt to open socket
            //
            // Don't want to hang around. If its taking more than 1 second
            // something is very wrong!
            if ($this->openSocket === false) {
                $this->openSocket = stream_socket_client($this->socket, $errno, $errstr, 1);
            }

            // Throw exception if unable to connect
            if ($this->openSocket === false) {
                throw new ReactEngineException('Unable to connect to socket');
            }

            // Chunk data to support sending large strings through socket and
            // always pass length of data as first response
            $json   = json_encode($data);
            $json   = strlen($json).'|'.$json;
            $buffer = str_split($json, 8192);

            foreach ($buffer as $chunk) {
                if (fwrite($this->openSocket, $chunk) === false) {
                    throw new ReactEngineException('Unable to write to socket');
                }
            }

            // Read back rendered content
            $buffer = '';
            while (! feof($this->openSocket)) {
                $return = fread($this->openSocket, 8192);

                // Throw exception if unable to read
                if (! $return) {

                    // Continue rather than throw an exception if there
                    // isn't actually any data
                    $meta = stream_get_meta_data($this->openSocket);
                    if ($meta['unread_bytes'] < 1) {
                        continue;
                    }

                    throw new ReactEngineException('Unable to read from socket');
                }

                // Add to buffer
                $buffer .= $return;
                
                // If end buffer is a pipe, break loop
                if (substr($buffer, -2) === '||') {
                    break;
                }
            }
            
            // If an exception has been returned, throw at this side
            if (substr($buffer, 0, 3) === 'e||') {
                throw new ReactEngineException(substr($buffer, 3, -2));
            }
            
            // Return buffer
            return $buffer;

        // Re-throw any exceptions as engine exceptions
        } catch (Exception $e) {
            throw new ReactEngineException($e);
        }
    }
}
