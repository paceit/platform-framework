<?php namespace Platform\Framework\View\Engines;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use RuntimeException;

class ReactEngineException extends RuntimeException
{
    //
}
