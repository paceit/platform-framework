<?php namespace Platform\Framework\Transformers;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use League\Fractal\TransformerAbstract as RootTransformerAbstract;

class TransformerAbstract extends RootTransformerAbstract
{
    /**
     * Resource type to use when transforming.
     *
     * @var string
     */
    protected $resource = false;

    /**
     * Return resource type.
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }
}
