<?php namespace Platform\Framework\Transformers;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

class PassthroughTransformer extends TransformerAbstract
{
    /**
     * This transformer is used when no valid transformer is available. It's
     * needed to we can still support the data serailizer.
     */
    public function transform($data)
    {
        return $data;
    }
}
