<?php namespace Platform\Framework\Concerns;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */


trait SessionBindings
{
    /**
     * Add session and cookie to available container bindings.
     *
     * @return void
     */
    public function __construct()
    {
        $this->availableBindings['cookie'] = 'registerCookieBindings';
        $this->availableBindings['Illuminate\Contracts\Cookie\Factory'] = 'registerCookieBindings';
        $this->availableBindings['Illuminate\Contracts\Cookie\QueueingFactory'] = 'registerCookieBindings';

        $this->availableBindings['session'] = 'registerSessionBindings';
        $this->availableBindings['session.store'] = 'registerSessionBindings';
        $this->availableBindings['Illuminate\Session\SessionManager'] = 'registerSessionBindings';
    }

    /**
     * Register the facades for the application.
     *
     * @return void
     */
    public function withFacades($aliases = true, $userAliases = [])
    {
        parent::withFacades($aliases, $userAliases);

        if (! static::$aliasesRegistered) {
            class_alias('Illuminate\Support\Facades\Cookie',  'Cookie');
            class_alias('Illuminate\Support\Facades\Session', 'Session');
        }
    }

    /**
     * Register container bindings for the application.
     *
     * @return void
     */
    protected function registerCookieBindings()
    {
        $this->singleton('cookie', function () {
            return $this->loadComponent('session', 'Illuminate\Cookie\CookieServiceProvider', 'cookie');
        });
    }

    /**
     * Register container bindings for the application.
     *
     * @return void
     */
    protected function registerSessionBindings()
    {
        $this->singleton('session', function () {
            return $this->loadComponent('session', 'Illuminate\Session\SessionServiceProvider');
        });
        $this->singleton('session.store', function () {
            return $this->loadComponent('session', 'Illuminate\Session\SessionServiceProvider', 'session.store');
        });
    }

    /**
     * Register the core container aliases.
     *
     * @return void
     */
    protected function registerContainerAliases()
    {
        $this->aliases['Illuminate\Contracts\Cookie\Factory'] = 'cookie';
        $this->aliases['Illuminate\Contracts\Cookie\QueueingFactory'] = 'cookie';
        $this->aliases['Illuminate\Session\SessionManager'] = 'session';
    }
}
