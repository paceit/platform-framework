<?php namespace Platform\Framework\Concerns;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Closure;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

trait RoutesRequests
{
    /**
     * API route prefix.
     *
     * @var string
     */
    protected $apiPrefix = 'api';

    /**
     * Indicates if the current application has API loaded.
     *
     * @var bool
     */
    protected $apiRegistered = false;

    /**
     * Whether request is internal.
     *
     * @var bool
     */
    protected $internalRequest = false;

    /**
     * Named API routes.
     *
     * @var array
     */
    protected $namedApiRoutes = [];

    /**
     * Add API available container bindings.
     *
     * @return void
     */
    public function __construct()
    {
        $this->availableBindings['api'] = 'registerApiBindings';
        $this->availableBindings['Platform\Framework\Contracts\Api'] = 'registerApiBindings';
    }

    /**
     * Return status of API.
     *
     * @return bool
     */
    public function apiActive()
    {
        return $this->apiRegistered;
    }

    /**
     * Enable API support.
     *
     * @param  string $prefix
     * @return void
     */
    public function withApi()
    {
        $this->apiRegistered = true;
    }

    /**
     * Return current API prefix.
     *
     * @return mixed
     */
    public function getApiPrefix()
    {
        return $this->apiPrefix;
    }

    /**
     * Set API prefix.
     *
     * @param  mixed $prefix
     * @return bool
     */
    public function setApiPrefix($prefix)
    {
        $this->apiPrefix = $prefix;
    }

    /**
     * Return named API routes.
     * 
     * @return array
     */
    public function getNamedApiRoutes()
    {
        return $this->namedApiRoutes;
    }

    /**
     * Register a service provider with the application.
     *
     * @param  \Illuminate\Support\ServiceProvider|string $provider
     * @param  array $options
     * @param  bool  $force
     * @return \Illuminate\Support\ServiceProvider
     */
    public function register($provider, $options = false, $force = false)
    {
        // If no options are set, call parent register
        if ($options === false) {
            return parent::register($provider);
        }

        $this->group($options, function($app) use($provider) {
            $app->register($provider);
        });

        // Reset group attributes
        $this->groupAttributes = null;
    }

    /**
     * Register a set of routes with a set of shared attributes.
     *
     * @param  array   $attributes
     * @param  Closure $callback
     * @param  boolean $reversePrefix
     * @return void
     */
    public function group(array $attributes, Closure $callback, $reversePrefix = false)
    {
        // Convert singular handler to multiple handlers
        if (isset($attributes['handler'])) {
            $attributes['handlers'] = [$attributes['handler']];
            unset($attributes['handler']);
        }

        // Call parent function
        parent::group($attributes, $callback);
    }

    /**
     * Set whether current request is internal.
     *
     * @param  bool $status
     * @return bool
     */
    public function setInternalRequest($status = true)
    {
        $this->internalRequest = $status === true;
    }

    /**
     * Add a route to the collection.
     *
     * @param  string  $method
     * @param  string  $uri
     * @param  mixed  $action
     */
    public function addRoute($method, $uri, $action)
    {
        $action = $this->parseAction($action);

        // Convert singular handler to multiple handlers
        if (isset($action['handler'])) {
            $action['handlers'] = [$action['handler']];
            unset($action['handler']);
        }

        // Add API route
        if ($this->apiRegistered) {
            $this->addApiRoute($method, $uri, $action);

            // If we are only serving API requests, return
            if (! $this->apiPrefix) {
                return;
            }
        }

        // If route only serves API requests, return
        if (isset($action['data']) && ! isset($action['uses'])) {
            return;
        }

        // Add standard route
        $action['api'] = false;
        parent::addRoute($method, $uri, $action);

        // Store path in action so we can use in react engine
        end($this->routes);
        $path = key($this->routes);
        $this->routes[$path]['action']['path'] = $path;
    }

    /**
     * Add an API route to the collection.
     *
     * @param  string  $method
     * @param  string  $uri
     * @param  mixed  $action
     */
    protected function addApiRoute($method, $uri, $action)
    {
        $action = $this->parseAction($action);

        // Skip if route does not specify a data controller
        if (! isset($action['data'])) {
            return;
        }

        // Remove unnecessary data
        unset($action['handlers']);
        unset($action['template']);

        // Add prefix to named routes
        if (isset($action['as']) && strpos($action['as'], 'api::') !== 0) {
            $action['as'] = 'api::'.$action['as'];
        }

        $action['uses'] = $action['data'];
        $action['api']  = true;

        // If we have an API prefix, create group to add
        // a root prefix
        if ($this->apiPrefix) {
            $this->group([
                'root_prefix' => $this->apiPrefix,
            ], function() use($method, $uri, $action) {
                parent::addRoute($method, $uri, $action);
            });
        }

        // If we have no API prefix, create the route as normal
        else {
            parent::addRoute($method, $uri, $action);
        }

        // If route is named, add to named routes
        if (isset($action['as'])) {
            $attributes = $this->hasGroupStack() ? $this->mergeWithLastGroup([]) : null;
            $this->namedApiRoutes[$action['as']] = $this->mergeGroupAttributes($action, $attributes);
        }
    }

    /**
     * Call the Closure on the array based route.
     *
     * @param  array $routeInfo
     * @return mixed
     */
    protected function callActionOnArrayBasedRoute($routeInfo)
    {
        // In case any of our middleware has updated the route
        // info, resync with request
        return parent::callActionOnArrayBasedRoute(
            $this['request']->route()
        );
    }

    /**
     * Call the callable for a controller action with the given parameters.
     *
     * @param  array  $callable
     * @param  array $parameters
     * @return mixed
     */
    protected function callControllerCallable(callable $callable, array $parameters = [])
    {
        $action = $this->currentRoute[1];

        // Call controller
        try {
            $response = $this->call($callable, $parameters);

            // If this is an API request, transform response
            if ($action['api']) {
                $response = $this['api']->transform($response);
            }

            return $this->prepareResponse($response);

        // Catch HTTP response exceptions
        } catch (HttpResponseException $e) {

            // If this is an API request, transform response and
            // return as normal
            if ($action['api']) {
                $response = $e->getResponse();

                // If exception is an instance of JSON response,
                // transform and return
                if ($response instanceof JsonResponse) {
                    $response->setData(
                        $this['api']->transform(
                            (array) $response->getData()
                        )
                    );

                    return $this->prepareResponse($response);
                }
            }

            // Return exception response
            return $e->getResponse();
        }
    }

    /**
     * Format the prefix for the new group attributes.
     *
     * @param  array  $new
     * @param  array  $old
     * @return string|null
     */
    protected static function formatGroupPrefix($new, $old)
    {
        // Retrieve old prefix
        $oldPrefix = null;
        if (isset($old['prefix'])) {
            $oldPrefix = $old['prefix'];
        }

        // Add new prefix
        if (isset($new['prefix'])) {
            return trim($oldPrefix, '/').'/'.trim($new['prefix'], '/');
        }

        // Add new root prefix
        if (isset($new['root_prefix'])) {
            return trim($new['root_prefix'], '/').'/'.trim($oldPrefix, '/');
        }

        return $oldPrefix;
    }

    /**
     * Handle a route found by the dispatcher. This is important
     * as we may be handling multiple requests internally.
     *
     * @param  array  $routeInfo
     * @return mixed
     */
    protected function handleFoundRoute($routeInfo)
    {
        // Update route resolver
        $this['request']->setRouteResolver(function()
        {
            return $this->currentRoute;
        });

        return parent::handleFoundRoute($routeInfo);
    }

    /**
     * Merge the group attributes into the action.
     *
     * @param  array $action
     * @param  array $attributes The group attributes
     * @return array
     */
    protected function mergeGroupAttributes(array $action, array $attributes)
    {
        // If we have attributes, merge into action
        if (isset($attributes)) {

            // If this isn't an API route, merge react attributes
            if (! $action['api']) {

                // Add layouts
                if (isset($attributes['handlers'])) {
                    if (isset($action['handlers'])) {
                        $action['handlers'] = array_merge($action['handlers'], $attributes['handlers']);
                    } else {
                        $action['handlers'] = $attributes['handlers'];
                    }
                }

                // Add template
                if (isset($attributes['template'])) {
                    $action['template'] = $attributes['template'];
                }
            }

            // Add package
            if (isset($attributes['package'])) {
                $action['package'] = $attributes['package'];
            }
        }

        return parent::mergeGroupAttributes($action, $attributes);
    }

    /**
     * Merge the namespace group into the action.
     *
     * @param  array  $action
     * @param  string $namespace
     * @return array
     */
    protected function mergeNamespaceGroup(array $action, $namespace = null)
    {
        if (isset($namespace)) {

            // Set data namespace
            if (isset($action['data'])) {
                $action['data'] = $namespace.'\\'.$action['data'];
            }

            // Set controller namespace
            if (isset($action['uses'])) {
                $action['uses'] = $namespace.'\\'.$action['uses'];
            }
        }

        return $action;
    }

    /**
     * Register container bindings for the application.
     *
     * @return void
     */
    protected function registerApiBindings()
    {
        $this->singleton('api', 'Platform\Framework\Api\Api');
    }

    /**
     * Register the core container aliases.
     *
     * @return void
     */
    protected function registerContainerAliases()
    {
        $this->aliases['Platform\Framework\Contracts\Api'] = 'api';
    }
}
