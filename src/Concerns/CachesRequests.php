<?php namespace Platform\Framework\Concerns;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */


trait CachesRequests
{
    /**
     * Whether to set cache header.
     *
     * @var bool
     */
    protected $cache = false;

    /**
     * Add cache middleware on construct.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([
            'Platform\Framework\Http\Middleware\AddCacheHeaders'
        ]);
    }

    /**
     * Return cache status.
     *
     * @return void
     */
    public function cacheActive()
    {
        return $this->cache;
    }

    /**
     * Enable cache.
     *
     * @return void
     */
    public function enableCache()
    {
        // If this is an internal request, skip
        if ($this->internalRequest === true) {
            return;
        }

        // If we in local mode, never enable cache
        if (env('PLATFORM_ENV', 'production') === 'local') {
            return;
        }

        $this->cache = true;
    }
}
