<?php namespace Platform\Framework\Concerns;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */


trait ApplicationPermissions
{
    /**
     * Cached permissions.
     * 
     * @var array
     */
    protected $permissions = [];

    /**
     * Retrieve access and permissions for requested result.
     * Purposely not using eloquent as models might be
     * in different namespaces depending on project.
     * 
     * @param  string $routeName
     * @return array
     */
    public function getPermissions($routeName = null)
    {
        // If no name has been passed, use current request route
        if (is_null($routeName)) {
            $route = $this['request']->getRouteResolver();
            $route = $route(); // Returned as a closure
            $route = $route[1];

            // If route is not named return false
            if (! isset($route['as'])) {
                return false;
            }

            $routeName = $route['as'];
        }

        // Remove API prefix
        if (strpos($routeName, 'api::') === 0) {
            $routeName = substr($routeName, 5);
        }

        // Check for cache
        if (isset($this->permissions[$routeName])) {
            return $this->permissions[$routeName];
        }

        $db    = $this['db'];
        $route = $db->table('routes')->where('name', $routeName)->first(['id', 'public']);
        $permissionRecords = [];

        // If we can't find route record, assume no access
        if (is_null($route)) {
            $public = false;
        } else {

            // Default access to route record setting
            $public = $route->public;

            // If user is logged in, check permissions
            $auth = $this['auth'];
            if ($auth->check()) {

                // Retrieve groups
                $groupIds = $auth->user()
                    ->groups()
                    ->select('groups.id')
                    ->pluck('id');

                // If route is not public, do any of groups have access?
                if ($public === false) {
                    $public = $db->table('group_route')
                        ->whereIn('group_id', $groupIds)
                        ->where('route_id', $route->id)
                        ->count() > 0;
                }

                // If we have access, check for specific permissions
                if ($public) {
                    $records = $db->table('group_route_route_permission')
                        ->whereIn('group_id', $groupIds)
                        ->where('route_id', $route->id)
                        ->get(['route_permission_id', 'value']);

                    // Loop and update permissions
                    foreach ($records as $record) {

                        // If already set and value is true, skip
                        if (isset($permissionRecords[$record->route_permission_id])) {
                            $permissionRecord = $permissionRecords[$record->route_permission_id];

                            if ($permissionRecord['value'] === true) {
                                continue;
                            }
                        }

                        $permissionRecords[$record->route_permission_id]['value'] = $record->value;
                    }
                }
            }

            // Retrieve permissions and parse into usable format
            $records = $db->table('route_permissions')
                ->where('route_id', $route->id)
                ->get(['route_permissions.id', 'permission', 'default']);

            foreach ($records as $record) {

                // If value already exists, only add permission label
                if (isset($permissionRecords[$record->id])) {
                    $permissionRecords[$record->id]['permission'] = $record->permission;
                    continue;
                }

                $permissionRecords[$record->id] = [
                    'permission' => $record->permission,
                    'value'      => $record->default,
                ];
            }
        }

        // Build permissions array
        $permissions = ['public' => $public];

        // If we have any permission records, add to array
        if (count($permissionRecords) > 0) {
            foreach ($permissionRecords as $record) {
                $permissions[$record['permission']] = $record['value'];
            }
        }

        // Cache permissions and return
        $this->permissions[$routeName] = $permissions;
        return $permissions;
    }
}
