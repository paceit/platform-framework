<?php namespace Platform\Framework\Api;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use RuntimeException;

class DataException extends RuntimeException
{
    //
}
