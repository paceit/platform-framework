<?php namespace Platform\Framework\Api;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Illuminate\Container\Container;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Platform\Framework\Contracts\Api as ApiContract;
use Platform\Framework\Transformers\PassthroughTransformer;

class Api implements ApiContract
{
    /**
     * Current application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

     /**
     * Default collection resource handler.
     *
     * @var string
     */
    protected $defaultCollectionResource = Collection::class;

    /**
     * Default item resource handler.
     *
     * @var string
     */
    protected $defaultItemResource = Item::class;

    /**
     * Default transformer.
     * 
     * @var string
     */
    protected $defaultTransformer = PassthroughTransformer::class;

    /**
     * Selected resource handler.
     *
     * @var \League\Fractal\Resource\ResourceInterface
     */
    protected $resource;

    /**
     * Selected transformer.
     * 
     * @var \League\Fractal\TransformerAbstract
     */
    protected $transformer;

    /**
     * Registered transformers.
     *
     * @var array
     */
    protected $transformers = [];

    /**
     * Initalise API library.
     *
     * @param  \Illuminate\Container\Container $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * Return API data for current route.
     *
     * @param  string $name
     * @param  array  $params
     * @param  array  $request
     * @param  bool   $transform
     * @return mixed
     */
    public function getRouteData($name = null, $params = null, $request = null, $transform = true)
    {
        // Set internal request and retrieve route config
        $this->app->setInternalRequest();
        $action = $this->getRouteConfig($name);

        // Validate this is actually an API route
        if (! isset($action['data'])) {
            throw new DataException('No API controller specified on current route');
        }

        // If no params have been passed, retrieve from current request
        if (is_null($params)) {
            $route  = $this->app['request']->getRouteResolver();
            $params = $route()[2];
        }

        // Run controller method
        try {
            list($controller, $method) = explode('@', $action['data']);

            // Build controller instance
            if (! method_exists($instance = $this->app[$controller], $method)) {
                throw new DataException('Invalid API route, unknown method');
            }

            // Update system for new request
            $restore = $this->newRequest($request);

            // Attempt to call controller method
            try {
                $response = $this->app->call([$instance, $method], $params);
            }

            // Catch any HTTP exceptions
            catch(HttpResponseException $e) {
                $response = $e->getResponse();

                // If exception is an instance of JSON response, extract data
                if ($response instanceOf JsonResponse) {
                    $response = (array) $response->getData();
                }

                // Otherwise rethrow exception
                else {
                    throw $e;
                }
            }

            // Tranform response
            if ($transform) {
                $response = $this->transform($response);
            }

            // Restore handlers
            foreach ($restore['handlers'] as $handler => $value) {
                $this->$handler = $value;
            }

            // Restore request
            if (! is_null($restore['request'])) {
                $this->app['request']->replace($restore['request']);
            }

            // Add permssions to response
            if (isset($action['as'])) {
                $permissions = $this->app->getPermissions($action['as']);

                // Add to response
                if (! isset($response['meta'])) {
                    $response['meta'] = ['permissions' => $permissions];
                } else {
                    $response['meta']['permissions'] = $permissions;
                }
            }

            // Unset internal request and return response
            $this->app->setInternalRequest(false);
            return $response;
        }

        // Catch any HTTP errors
        catch (HttpResponseException $e) {
            $this->app->setInternalRequest(false);
            return $e;
        }
    }

    /**
     * Run the passed response through a matching transformer.
     * 
     * @param  mixed $response
     * @return mixed
     */
    public function transform($response)
    {
        $pagination = false;

        // Handle objects
        if (is_object($response)) {

            // Are we handling pagination?
            $implements = class_implements($response);
            $pagination = isset($implements['Illuminate\Contracts\Pagination\Paginator']);

            // If we are handling pagination, we should actually run the
            // transformer against the collection
            if ($pagination) {
                $pagination = $response;
                $response   = $response->getCollection();
            }

            // If no transformer has been set, check if any registered
            // transfomers are valid for this response
            if (! $this->transformer) {
                foreach ($this->transformers as $tranform => $transformer) {

                    // If class implements or extends, use this transformer
                    if (isset($implements[$transform]) || is_a($response, $transform)) {
                        $this->transformer = new $transformer;
                        break;
                    }
                }
            }
        }

        // If we don't have a transformer, set default
        if (! $this->transformer) {
            $this->transformer = new $this->defaultTransformer;
        }

        // If we have no resource type, determine handler to use
        if (! $this->resource) {
            switch(true) {

                // Does the transformer specify a resource handler?
                case $handler = $this->transformer->getResource():
                    $this->resource = $handler;
                    break;

                // If we are processing pagination, or processing a numeric
                // array, handle using collection
                case $pagination:
                case is_array($response) && isset($response[0]):
                    $this->resource = $this->defaultCollectionResource;
                    break;

                // Default to item
                default:
                    $this->resource = $this->defaultItemResource;
            }
        }

        // Build a resource from the response
        $resource = new $this->resource($response, $this->transformer);

        // If we handling pagination, set paginator on resource
        if ($pagination) {
            $resource->setPaginator(new IlluminatePaginatorAdapter($pagination));
        }

        // Reset handlers
        $this->defaultCollectionResource = Collection::class;
        $this->defaultItemResource       = Item::class;
        $this->defaultTransformer        = PassthroughTransformer::class;
        $this->resource                  = null;
        $this->transformer               = null;
        $this->transformers              = [];

        // Transform and return
        return (new Manager)->createData($resource)->toArray();
    }

    /**
     * Register transformer.
     *
     * @param  string $transform
     * @param  string $transform
     * @return void
     */
    public function registerTransformer($transform, $transformer)
    {
        $this->transformers[$transform] = $transformer;
    }

    /**
     * Set default collection resource handler.
     * 
     * @param  string $resource
     * @return void
     */
    public function setDefaultCollectionResource($resource)
    {
        $this->defaultCollectionResource = $resource;
    }

    /**
     * Set default item resource handler.
     * 
     * @param  string $resource
     * @return void
     */
    public function setDefaultItemResource($resource)
    {
        $this->defaultCollectionResource = $resource;
    }

    /**
     * Set transformer to use when no specific transformer is available.
     * 
     * @param  string $transformer
     * @return void
     */
    public function setDefaultTransformer($transformer)
    {
        $this->defaultTransformer = $transformer;
    }

    /**
     * Set resource handler for current request.
     *
     * @param  string $resoruce
     * @return void
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Set transformer to use for current request.
     * 
     * @param  \League\Fractal\TransformerAbstract $transformer
     * @return void
     */
    public function setTransformer(TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Return route config.
     *
     * @param  string $name
     * @return array
     */
    protected function getRouteConfig($name = null)
    {
        // If name is null, return current route action
        if (is_null($name)) {
            $route  = $this->app['request']->getRouteResolver();
            $route  = $route(); // Returned as a closure

            return $route[1];
        }

        // Add name prefix
        if (strpos($name, 'api::') !== 0) {
            $name = 'api::'.$name;
        }

        // Throw exception if route does not exist
        $namedApiRoutes = $this->app->getNamedApiRoutes();
        if (! isset($namedApiRoutes[$name])) {
            throw new DataException('Unknown API route ('.$name.')');
        }

        return $namedApiRoutes[$name];
    }

    /**
     * Setup system for new request.
     *
     * @param  array $request
     * @return array
     */
    protected function newRequest($request)
    {
        $handlers = [];
        $orig = null;

        // Add handlers
        foreach([
            'defaultCollectionResource',
            'defaultItemResource',
            'defaultTransformer',
            'resource',
            'transformer',
            'transformers',
        ] as $handler) {
            $handlers[$handler] = $this->$handler;
        }

        // Update request
        if (! is_null($request)) {
            $req  = $this->app['request'];
            $orig = $req->all();
            $req->replace($request);

            // Parse into format used for restore
            $orig = ['request' => $orig];
        }

        // Reset handlers
        $this->defaultCollectionResource = Collection::class;
        $this->defaultItemResource       = Item::class;
        $this->defaultTransformer        = PassthroughTransformer::class;
        $this->resource                  = null;
        $this->transformer               = null;
        $this->transformers              = [];

        // Return restore array
        return [
            'handlers' => $handlers,
            'request'  => $orig,
        ];
    }
}
