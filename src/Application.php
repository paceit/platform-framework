<?php namespace Platform\Framework;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Laravel\Lumen\Application as RootApplication;
use Platform\Framework\Concerns\ApplicationPermissions;
use Platform\Framework\Concerns\CachesRequests;
use Platform\Framework\Concerns\RoutesRequests;
use Platform\Framework\Concerns\SessionBindings;

class Application extends RootApplication
{
    use ApplicationPermissions, CachesRequests, RoutesRequests, SessionBindings {
        CachesRequests::__construct as cachesRequestsConstruct;
        RoutesRequests::__construct as routesRequestsConstruct;
        SessionBindings::__construct as sessionBindingsConstruct;

        RoutesRequests::registerContainerAliases as routesRequestsRegisterContainerAliases;
        SessionBindings::registerContainerAliases as sessionBindingsRegisterContainerAliases;
    }

    /**
     * Run trait constructors.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cachesRequestsConstruct();
        $this->routesRequestsConstruct();
        $this->sessionBindingsConstruct();

        $this->middleware([
            'Platform\Framework\Http\Middleware\AddRequestUrlHeaders'
        ]);

        parent::__construct();
    }

    /**
     * Get the path to the given configuration file.
     *
     * @param  string|null  $name
     * @return string
     */
    public function getConfigurationPath($name = null)
    {
        // If we have no name, return folder path
        if (! $name) {
            $appConfigDir = $this->basePath('config').'/';
            if (file_exists($appConfigDir)) {
                return $appConfigDir;
            }

            return __DIR__.'/../config/';
        }

        // Return specific config path
        $appConfigPath = $this->basePath('config').'/'.$name.'.php';
        if (file_exists($appConfigPath)) {
            return $appConfigPath;
        }

        // Check for config in default platform configs
        if (file_exists($path = __DIR__.'/../config/'.$name.'.php')) {
            return $path;
        }

        return parent::getConfigurationPath($name);
    }

    /**
     * Register the core container aliases.
     *
     * @return void
     */
    protected function registerContainerAliases()
    {
        parent::registerContainerAliases();

        $this->routesRequestsRegisterContainerAliases();
        $this->sessionBindingsRegisterContainerAliases();
    }

    /**
     * Register container bindings for the application.
     *
     * @return void
     */
    protected function registerFilesBindings()
    {
        parent::registerFilesBindings();
        
        $this->singleton('filesystem', function () {
            return $this->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
        });
    }

    /**
     * Override registering of view bindings to use our own version, where
     * we add support for react views.
     *
     * @return void
     */
    protected function registerViewBindings()
    {
        $this->singleton('view', function ()
        {
            return $this->loadComponent('view', 'Platform\Framework\Providers\ViewServiceProvider');
        });
    }
}
