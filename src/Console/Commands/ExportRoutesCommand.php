<?php namespace Platform\Framework\Console\Commands;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use ReflectionMethod;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;

class ExportRoutesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'pace-framework:export-routes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export route data for use with react';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $routes = $this->laravel->getRoutes();

        // Create root for matching routes
        $matches = [
            'children' => [],
            'routes'   => [],
        ];

        // Sort by key
        ksort($routes);

        // Loop through routes
        foreach ($routes as $route) {
            $action = $route['action'];

            // Check if we should skip route
            if ($this->skipRoute($route)) {
                continue;
            }

            // Add to matching routes
            $current = &$matches;
            if (isset($action['handlers'])) {
                foreach ($action['handlers'] as $handler) {

                    // Create array key
                    if (! isset($current['children'][$handler])) {
                        $current['children'][$handler] = [
                            'children' => [],
                            'routes'   => [],
                        ];
                    }

                    $current = &$current['children'][$handler];
                }
            }

            // Parse name
            if (isset($action['as'])) {
                $name = $action['as'];
            } else {
                $name = null;
            }

            // Add route
            $current['routes'][] = [
                'name' => $name,
                'path' => $route['uri'],
                'uses' => $action['uses'],
            ];
        }

        // Parse route components JSX
        $matches = $this->parseComponents($matches);
        $jsx     = $this->buildJSX($matches);

        // Parse output
        $contents = 'var React  = require(\'react\');
var Router = require(\'react-router\');
var Route  = Router.Route;

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

module.exports = ('.$jsx.'
);
';

        // Save to file
        $output = $this->argument('output');
        if (! file_put_contents($output, $contents)) {
            $this->error('Unable to save routes file');
        }
    }

    /**
     * Return specified indentation.
     *
     * @param  string $string
     * @param  int    $indent
     * @return string
     */
    protected function addIndent($indent)
    {
        $string = '';

        for ($i = 0; $i < $indent; $i++) {
            $string .= "\t";
        }

        return $string;
    }

    /**
     * Build JSX from parsed routes.
     *
     * @param  array  $routes
     * @param  string $handler
     * @param  string $indent
     * @return string
     */
    protected function buildJSX($routes, $handler = null, $indent = 1)
    {
        // Start route block
        $jsx = "\n".$this->addIndent($indent).'<Route';
        if (! is_null($handler)) {
            $jsx .= ' handler={require(\''.$handler.'\')}';
        }
        $jsx .= '>';

        // Add routes
        foreach ($routes['routes'] as $route) {
            $jsx .= "\n".$this->addIndent($indent + 1).'<Route';

            // If we have a name defined, add to route
            if (! is_null($route['name'])) {
                $jsx .= ' name="'.$route['name'].'"';
            }

            // Convert path parameters to be compatible
            // with react router
            $route['path'] = str_replace(['{', '}'], [':', ''], $route['path']);

            // Add path
            $jsx .= ' path="'.$route['path'].'"';

            // Add handler
            $jsx .= ' handler={require(\''.$route['uses'].'\')} />';
        }

        // Add any children
        foreach ($routes['children'] as $handler => $children) {
            $jsx .= $this->buildJSX($children, $handler, $indent + 1);
        }

        // End route block and return code
        $jsx .= "\n".$this->addIndent($indent).'</Route>';
        return $jsx;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['output', InputArgument::REQUIRED, 'File to output routes to.'],
            ['package', InputArgument::OPTIONAL, 'Package containing routes to export.'],
        ];
    }

    /**
     * Parse react components to use for each matching route.
     *
     * @param  array $matches
     * @return void
     */
    protected function parseComponents($matches)
    {
        $finder = $this->laravel['view.finder'];

        // Parse children
        foreach ($matches['children'] as $handler => $config) {

            // Parse handler path
            $key = $finder->find($handler);
            if (! is_file($key)) {
                throw new ReactEngineException("Handler path invalid ({$handler})");
            }

            // Convert to relative position from router
            $key = $this->relativePath($this->argument('output'), $key);

            // Parse child routes and remove original child
            $matches['children'][$key] = $this->parseComponents($config);
            unset($matches['children'][$handler]);
        }

        // Parse routes
        foreach ($matches['routes'] as $i => $route) {
            if (strpos($route['uses'], '@') === false) {
                $controller = $route['uses'];
                $method = '__invoke';
            } else {
                list($controller, $method) = explode('@', $route['uses']);
            }
            $method = new ReflectionMethod($controller, $method);

            // Retrieve code
            $file   = file($method->getFileName());
            $start  = $method->getStartLine();
            $end    = $method->getEndLine();
            $source = implode('', array_slice($file, $start, $end - $start));

            // If route does not contain a react component, remove match
            if (strpos($source, 'react(') === false) {
                unset($matches['routes'][$i]);
                continue;
            }

            // Retrieve react component
            preg_match('/react\([\'"][^\'"]+[\'"]/', $source, $component);
            if (empty($component)) {
                throw new RuntimeException('Unable to parse react component from controller method ('.$route['uses'].')');
            }
            $component = substr($component[0], 7, -1);

            // Parse view path
            $path = $finder->find($component);
            if (! is_file($path)) {
                throw new ReactEngineException("Component path invalid ({$path})");
            }

            // Convert to relative position from router
            $path = $this->relativePath($this->argument('output'), $path);
            $matches['routes'][$i]['uses'] = $path;
        }

        return $matches;
    }

    /**
     * Find the relative file system path between two files.
     *
     * @param  string $from
     * @param  string $to
     * @return string
     */
    protected function relativePath($from, $to)
    {
        $from    = realpath(dirname($from)).'/'.basename($from);
        $from    = explode('/', $from);
        $to      = explode('/', realpath($to));
        $relPath = $to;
        
        // Loop through path
        foreach ($from as $depth => $dir) {
        
            // Find first non-matching dir
            if ($dir === $to[$depth]) {

                // Ignore this directory
                array_shift($relPath);
            }

            // Get number of remaining dirs to $from
            else {
                $remaining = count($from) - $depth;

                // Add traversals up to first matching dir
                if ($remaining > 1) {
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath   = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    $relPath[0] = './' . $relPath[0];
                }
            }
        }

        // Return relative path
        return implode('/', $relPath);
    }

    /**
     * Determine whether passed route action should be skipped.
     *
     * @param  array $action
     * @return bool
     */
    protected function skipRoute($route)
    {
        $package = $this->argument('package');
        $action  = $route['action'];

        // If no package is specified, only process base routes
        if (is_null($package)) {
            if (isset($action['package']) && ! is_null($action['package'])) {
                return true;
            }
        }

        // If package is specified, only process package routes
        if (isset($action['package'])) {
            $packages = [$package, $action['package']];

            // Sanatize package names
            array_walk($packages, function(&$package)
            {
                $package = strtolower(str_replace(array('/', '\\'), '', $package));
            });

            if ($packages[0] !== $packages[1]) {
                return true;
            }
        }

        // Skip if not a GET route
        if ($route['method'] !== 'GET') {
            return true;
        }

        // Skip if API route or not using controller
        if ($action['api'] || ! isset($action['uses'])) {
            return true;
        }

        return false;
    }
}
