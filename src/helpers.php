<?php

/**
 * Platform: Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

use Illuminate\Container\Container;

if (! function_exists('build')) {
    /**
     * Get the path to a versioned build file.
     *
     * @param  string $file
     * @return string
     */
    function build($file)
    {
        static $manifest = null;

        // Retrieve manifest
        if (is_null($manifest)) {
            $path     = base_path('public').'/build/rev-manifest.json';
            $manifest = [];

            if (is_file($path)) {
                $contents = file_get_contents($path);
                $manifest = json_decode($contents, true);
            }
        }

        // Return built file
        if (isset($manifest[$file])) {

            // Use build base if available
            $buildBase = env('ASSET_BASE');
            if (! is_null($buildBase)) {
                $buildBase = rtrim($buildBase, '/');
                return $buildBase.'/build/'.$manifest[$file];
            }

            return 'build/'.$manifest[$file];
        }

        return $file;
    }
}

if (! function_exists('cookie')) {
    /**
     * Create a new cookie instance.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  int     $minutes
     * @param  string  $path
     * @param  string  $domain
     * @param  bool    $secure
     * @param  bool    $httpOnly
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    function cookie($name = null, $value = null, $minutes = 0, $path = null, $domain = null, $secure = false, $httpOnly = true)
    {
        $cookie = app('cookie');
        if (is_null($name)) {
            return $cookie;
        }
        return $cookie->make($name, $value, $minutes, $path, $domain, $secure, $httpOnly);
    }
}

if (! function_exists('get_parameter_value')) {
    /**
     * Retrieve parameter value.
     *
     * @param  string $code
     * @param  string $primary
     * @param  string $key
     * @return string|null
     */
    function get_parameter_value($code, $primary, $key)
    {
        // Query database
        $record = app('db')
            ->table('parameter_values')
            ->where('parameter_code', $code)
            ->where('primary', $primary)
            ->where('parameter_rule_key', $key)
            ->first(['value']);

        // If we have no value, return null
        if ($record === null) {
            return null;
        }

        return $record->value;
    }
}

if (! function_exists('is_react')) {
    /**
     * Return whether current request is a react react.
     *
     * @return bool
     */
    function is_react()
    {
        $request = app('request');

        // If request has 
        if ($request->exists('react')) {
            return true;
        }

        // Check powered by header
        $powered = $request->header('X-Powered-By');
        return ! is_null($powered) && stripos($powered, 'React') === 0;
    }
}

if (! function_exists('react')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $header
     * @param  string $primary
     * @return \Illuminate\View\View
     */
    function react($view = null, $data = array(), $header = array(), $primary = 'Global')
    {
        // Retrieve default header settings
        $defaults = [
            'title'            => get_parameter_value('HEADER', $primary, 'Title'),
            'meta_description' => get_parameter_value('HEADER', $primary, 'Description'),
            'meta_keywords'    => get_parameter_value('HEADER', $primary, 'Keywords')
        ];

        // Set defaults
        foreach (array_keys($defaults) as $key) {
            if (empty($header[$key])) {
                $header[$key] = $defaults[$key];
            }
        }

        // Add header to meta
        if (empty($data['meta'])) {
            $data['meta'] = ['header' => $header];
        } else {
            $data['meta']['header'] = $header;
        }

        // If request is from react, return variables only
        // (client does the rendering)
        if (is_react()) {

            // Convert object to array
            if ($data instanceof Arrayable) {
                $data = $data->toArray();
            }

            // Add header to meta
            if (empty($data['meta'])) {
                $data['meta'] = ['header' => $header];
            } else {
                $data['meta']['header'] = $header;
            }

            // Return data, response object will convert to JSON
            // and add correct content headers for us
            return $data;
        }

        // Return view object
        $data['_header'] = $header;
        return view($view, $data);
    }
}

if (! function_exists('session')) {
    /**
     * Get / set the specified session value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function session($key = null, $default = null)
    {
        if (is_null($key)) {
            return Container::getInstance()->make('session');
        }
        if (is_array($key)) {
            return Container::getInstance()->make('session')->put($key);
        }
        return Container::getInstance()->make('session')->get($key, $default);
    }
}
